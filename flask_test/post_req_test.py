from flask import Flask, jsonify, request
app = Flask(__name__)

@app.route('/')
def hello_world():
    return "hello_world!"

@app.route('/add_two_nums', methods=["POST"])
def add_two_nums():
    #get x,y values
    dataDict = request.get_json()
    # return jsonify(dataDict)
    if "y" not in dataDict:
        return "EROOR", 365
    x = dataDict["x"]
    y = dataDict["y"]
    
    #add z=x+y
    z = x +y

    #prepare a JSON "z"=z
    retJson = {
        "z":z
    }

    #return jsonify(map_prepared)
    return jsonify(retJson), 200

if __name__=="__main__":
    app.run()