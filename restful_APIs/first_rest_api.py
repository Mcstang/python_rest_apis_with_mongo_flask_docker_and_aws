from flask import Flask, jsonify, request
from flask_restful import Api, Resource


app = Flask(__name__)
api = Api(app)

class Add(Resource):
    def post(self):

        # step 01 - get posted data

        postedData = request.get_json()
        x = postedData("x")
        y = postedData("y")
        x = int(x)
        y = int(y)

        # step 02 - add the posted data
        ret = x+y
        retMap = {
            "message": ret,
            "status code":200
        }
        # step 03 - return the response 
        return jsonify(retMap)


class Sub(Resource):
    pass


class Div(Resource):
    pass

class Mul(Resource):
    pass


api.add_resource(Add, "/add")


@app.route('/')
def hello_world():
    return "hello_world!"

if __name__=="__main__":
    app.run(debug=True)